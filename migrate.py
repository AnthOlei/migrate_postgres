import pandas as pd
import sqlalchemy


#SET THESE VARIABLES UP
tables = ["table1"]
from_db_name = "scraped_products"
to_db_name = "swipeshop"


print(
    f'Beginning copying of tables {[table for table in tables]} in that order')

from_db = sqlalchemy.create_engine(
        f'postgresql+pygresql://user:password@host:port/{from_db_name}')
to_db = sqlalchemy.create_engine(
        f'postgresql+pygresql://user:password@host:post/{to_db_name}')    

for table in tables:
    print(f'transfering table data from {table}')

    query = f'SELECT * FROM public.{table}'
    df = pd.read_sql(query, from_db)
    df.to_sql(table, to_db, index=False, if_exists='append')

    print(f'{table} copied.')
print(f'data copied - referential integrity mantained.')

